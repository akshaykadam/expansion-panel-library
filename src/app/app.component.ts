import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'expansion-panel';
  descriptionArray: any[];

  data = { title: 'Patient Details', subtitle: 'John Smith' };

  ngOnInit() {
    this.descriptionArray = [
      { title: 'patient 1', desc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit 1.' }
    ];
  }
}
