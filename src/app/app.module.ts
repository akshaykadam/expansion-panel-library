import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ExpansionpanelModule } from 'expansionpanel';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ExpansionpanelModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
