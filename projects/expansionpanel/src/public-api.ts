/*
 * Public API Surface of expansionpanel
 */

export * from './lib/expansionpanel.service';
export * from './lib/expansionpanel.component';
export * from './lib/expansionpanel.module';
export * from './lib/title.component';
export * from './lib/description.component';
