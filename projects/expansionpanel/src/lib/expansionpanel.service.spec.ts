import { TestBed } from '@angular/core/testing';

import { ExpansionpanelService } from './expansionpanel.service';

describe('ExpansionpanelService', () => {
  let service: ExpansionpanelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExpansionpanelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
