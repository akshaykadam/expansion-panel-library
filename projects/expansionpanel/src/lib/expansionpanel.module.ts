import { NgModule } from '@angular/core';
import { ExpansionpanelComponent } from './expansionpanel.component';
import { ExpansiontitleComponent } from './title.component';
import { ExpansionDescriptionComponent } from './description.component';



@NgModule({
  declarations: [ExpansionpanelComponent, ExpansiontitleComponent, ExpansionDescriptionComponent],
  imports: [
  ],
  exports: [ExpansionpanelComponent, ExpansiontitleComponent, ExpansionDescriptionComponent]
})
export class ExpansionpanelModule { }
