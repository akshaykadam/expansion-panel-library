import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "ng-expansionpanel",
  template: `
    <div class="accordion">
      <div class="accordion-tab">
        <input
          id="toggle1"
          type="checkbox"
          class="accordion-toggle"
          name="toggle"
        />
        <label for="toggle1"
          ><ng-content select="[slot=title]"></ng-content
        ></label>
        <div class="accordion-content">
          <p>
            <ng-content select="[slot=description]"></ng-content>
          </p>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      * {
        margin: 0;
        padding: 0;
      }

      *,
      *:before,
      *:after {
        box-sizing: border-box;
      }

      html,
      body {
        height: 100%;
        padding: 0.5rem;
      }

      h1 {
        margin: 0 0 30px;
      }

      .accordion {
        width: 100%;
        background: #fff;
        -moz-box-shadow: 0 0 1px 1px #888;
        -webkit-box-shadow: 0 0 1px 1px#888;
        box-shadow: 0 0 1px 1px #888;
        border-top-left-radius: 0.1rem;
        border-top-right-radius: 0.1rem;
        border-bottom-left-radius: 0.1rem;
        border-bottom-right-radius: 0.1rem;
      }

      .accordion-tab > .accordion-toggle {
        position: absolute;
        display: none;
      }

      .accordion-tab > label {
        font-size: 16px;
        line-height: 24px;
        font-weight: 700;
        position: relative;
        display: block;
        padding: 13px 40px 13px 20px;
        border-top: 1px solid #ddd;
        cursor: pointer;
      }

      .accordion-tab > label:hover {
        background-color: #e4e4e4;
      }

      .accordion-tab > .accordion-content {
        max-height: 0;
        transition: all 0.2s;
        overflow: hidden;
      }

      .accordion-tab > label:after {
        content: "+";
        position: absolute;
        top: 13px;
        right: 20px;
        transition: transform 0.2s;
      }

      .accordion-tab > .accordion-toggle:checked ~ label:after {
        content: "-";
      }

      .accordion-tab > .accordion-toggle:checked ~ .accordion-content {
        max-height: 100vh;
      }

      .accordion-tab > .accordion-content p {
        margin: 15px 0;
        padding: 0 20px;
        font-size: 15px;
        line-height: 1.5;
      }
    `,
  ],
})
export class ExpansionpanelComponent implements OnInit {
  @Input() data: any;

  constructor() {}

  ngOnInit(): void {
    console.log(this.data);
  }
}
