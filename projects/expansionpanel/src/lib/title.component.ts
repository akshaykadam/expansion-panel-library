import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-expansiontitle',
  template: `
    <div>
      <ng-content></ng-content>
    </div>
  `,
  styles: [
    `
    `,
  ],
})
export class ExpansiontitleComponent implements OnInit {

  constructor() {}
    ngOnInit(): void {

    }
}
