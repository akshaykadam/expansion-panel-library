import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-expansiondescription',
  template: `
    <div>
      <ng-content></ng-content>
    </div>
  `,
  styles: [
    `
    `,
  ],
})
export class ExpansionDescriptionComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
  }
}
